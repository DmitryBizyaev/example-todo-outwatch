//// глобальная переменная для записи информации об используемой памяти
//let memoryCheckResult = "";

function doMultipleClicks(element, numberOfLoops) {
    for (let i = 0; i < numberOfLoops; i++) {
        element.click();
//        await new Promise(r => setTimeout(r, 1));
    }
}

function clickAllElements(elementsList, numberOfClicks) {
    for (element of elementsList) {
        doMultipleClicks(element, numberOfClicks);
    }
}

async function waitForElementToAppear(elementClassName, expectedNumberOfElements) {
    let breakCount = 0;
    let currentNumberOfElements = document.getElementsByClassName(elementClassName);
    while (currentNumberOfElements.length < expectedNumberOfElements) {
        // setTimeout используется для освобождения ресурсов для выполнения добавления элементов в DOM
        await new Promise(r => setTimeout(r, 1));
        currentNumberOfElements = document.getElementsByClassName(elementClassName);
        // защита от зацикливания
        breakCount++;
        if (breakCount > 10000) {
            console.log("Waiting for element timed out");
            break;
        }
    }
}

async function waitForElementToDisappear(elementClassName) {
    let breakCount = 0;
    let currentNumberOfElements = document.getElementsByClassName(elementClassName);
    while (currentNumberOfElements.length > 0) {
        // setTimeout используется для освобождения ресурсов для выполнения добавления элементов в DOM
        await new Promise(r => setTimeout(r, 1));
        currentNumberOfElements = document.getElementsByClassName(elementClassName);
        // защита от зацикливания
        breakCount++;
        if (breakCount > 10000) {
            console.log("Waiting for element timed out");
            break;
        }
    }
}

//function convertMemoryData(data) {
//    const totalMemory = new String(data.totalJSHeapSize);
//    const usedMemory = new String(data.usedJSHeapSize);
//    return totalMemory + " " + usedMemory + ",";
//}
//
//function runMemoryMeasurements() {
//    const interval = 100;
//    setTimeout(measureMemory, interval);
//}
//
//async function measureMemory() {
//    const memorySample = await window.performance.memory;
//    if (document.getElementsByClassName("test result title").length > 0) {
//        memoryCheckResult = memoryCheckResult + convertMemoryData(memorySample);
//    } else {
//        memoryCheckResult = memoryCheckResult + convertMemoryData(memorySample);
//        runMemoryMeasurements();
//    }
//}

function getResultHeader(testTime, listNum, noteNum) {
    const t = String(testTime);
    const l = String(listNum);
    const n = String(noteNum);
    return t + " " + l + " " + n + ",";
}

async function copyToClipboard() {
    const text = document.getElementById("test-result-data").value;
    await navigator.clipboard.writeText(text);

    console.log("Text copied");
    alert("Text copied");
}

function convertToMb(memory) {
    const currMem = Number(memory) / (1024 * 1024);
    return Math.round(currMem * 10) / 10;
}

async function getMilestoneData(timeStart, timeArray, memoryArray) {
    const currTime = new Date() - timeStart;
    const currMemorySample = await window.performance.memory;

    timeArray.push(String(currTime));
    memoryArray.push(String(currMemorySample.usedJSHeapSize));
}

function convertDataToText(lists, notes, timeArray, memoryArray) {
    const elementsText = String(lists) + "." + String(notes);
    const timeText = timeArray.join(".");
    const memText = memoryArray.join(".");
    return elementsText + "," + timeText + "," + memText;
}

async function test() {
    console.log("js-test: start");

    // начало отсчета времени и начальное значение используемой памяти
    const memoryStartSample = await window.performance.memory;
    const startTime = new Date();
    let timeData = [String(0)];
    let memoryData = [String(memoryStartSample.usedJSHeapSize)];

    // определение количества создаваемых элементов и режима удаления элементов
    const numberOfLists = document.getElementById("number-of-lists").value;
    const numberOfNotes = document.getElementById("number-of-notes").value;
    const removalMode = document.getElementById("removal-mode").value;

    // получение интерактивных элементов списка списков
    const listCreateButton = document.getElementsByClassName("buttonListCreate")[0];
    // создание определенного количества списков
    doMultipleClicks(listCreateButton, numberOfLists);
    // ожидание добавления списков в DOM
    await waitForElementToAppear("listOfElements", numberOfLists);

    await getMilestoneData(startTime, timeData, memoryData);
//    console.log("js-test: end of phase 1");

    // получение интерактивных элементов списков элементов
    const noteCreateButtons = Array.from(document.getElementsByClassName("buttonNoteCreate"));
    // создание определенного количества элементов в списках
    clickAllElements(noteCreateButtons, numberOfNotes);
    // ожидание добавления элементов в списки
    await waitForElementToAppear("buttonNoteRemove", numberOfLists * numberOfNotes);

    await getMilestoneData(startTime, timeData, memoryData);
//    console.log("js-test: end of phase 2");

    if (removalMode == "default") {
        // получение интерактивных элементов (кнопок удаления) элементов в списках
        const noteRemoveButtons = Array.from(document.getElementsByClassName("buttonNoteRemove"));
        // удаление элементов из списков
        clickAllElements(noteRemoveButtons, 1);
        // ожидание удаления элементов из списков
        await waitForElementToDisappear("buttonNoteRemove");

        await getMilestoneData(startTime, timeData, memoryData);

        // получение интерактивных элементов (кнопок удаления) списков
        const noteRemoveLists = Array.from(document.getElementsByClassName("buttonListRemove"));
        // удаление списков
        clickAllElements(noteRemoveLists, 1);
        // ожидание удаления списков
        await waitForElementToDisappear("buttonListRemove");
    } else {
        // получение интерактивных элементов (кнопок удаления) списков
        const noteRemoveLists = Array.from(document.getElementsByClassName("buttonListRemove"));
        // удаление списков
        clickAllElements(noteRemoveLists, 1);
        // ожидание удаления списков
        await waitForElementToDisappear("buttonListRemove");
    }

    // итоговые время выполнения теста и используемая память
    await getMilestoneData(startTime, timeData, memoryData);

//    ProfilerAgent.collectGarbage();
//    await new Promise(r => ProfilerAgent.collectGarbage());
    await new Promise(r => setTimeout(r, 2000));
    await getMilestoneData(startTime, timeData, memoryData);

    console.log("js-test: end");
    console.log(timeData);

    // создание текстов для элементов с результатами
    const resultTitleText = "Test computed";
    const resultTitleListText = " (" + String(numberOfLists) + " lists with ";
    const resultTitleNoteText = String(numberOfNotes) + " notes in each)";

    const resultTimeMsText = "Time elapsed: " + String(timeData.slice(-1));
    const resultTimeSText = " (" + String(Math.round(timeData.slice(-1) / 100) / 10) + " s)";

    const resultMemoryText = "Used memory: ";
    const resultMemoryStartText = String(convertToMb(memoryData[0])) + " Mb -> ";
    const resultMemoryEndText = String(convertToMb(memoryData.slice(-1))) + " Mb";

    const resultDataText = convertDataToText(numberOfLists, numberOfNotes, timeData, memoryData);

    // создание и добавление в DOM элемента с результатами
    const resultTitleElement = document.createElement("div");
    resultTitleElement.textContent = resultTitleText + resultTitleListText + resultTitleNoteText;
    resultTitleElement.className = "test-result";
    document.body.appendChild(resultTitleElement);

    await waitForElementToAppear("test-result", 1);

    // создание и добавление в DOM элемента с результатами (время)
    const resultTimeElement = document.createElement("div");
    resultTimeElement.textContent = resultTimeMsText + resultTimeSText;
    resultTimeElement.className = "test-result-time";
    document.getElementsByClassName("test-result")[0].appendChild(resultTimeElement);

    // создание и добавление в DOM элемента с результатами (память)
    const resultMemoryElement = document.createElement("div");
    resultMemoryElement.textContent = resultMemoryText + resultMemoryStartText + resultMemoryEndText;
    resultMemoryElement.className = "test-result-memory";
    document.getElementsByClassName("test-result")[0].appendChild(resultMemoryElement);

    // создание и добавление в DOM элемента с результатами для визуализации
    const resultDataElement = document.createElement("textarea");
    resultDataElement.setAttribute("style", "resize:none");
    resultDataElement.setAttribute("readonly", "");
    resultDataElement.setAttribute("id", "test-result-data");
    resultDataElement.className = "test-result-data";
    resultDataElement.textContent = resultDataText;
    document.getElementsByClassName("test-result")[0].appendChild(resultDataElement);

    await waitForElementToAppear("test-result-data", 1);

    // кнопка для быстрого копирования результатов
    const copyButtonDiv = document.createElement("div");
    copyButtonDiv.setAttribute("id", "copy-button-div");
    document.body.appendChild(copyButtonDiv);

    const copyButton = document.createElement("button");
    copyButton.textContent = "Copy text";
    copyButton.setAttribute("id", "copy-button");

    document.getElementById("copy-button-div").appendChild(copyButton);
    document.getElementById("copy-button").addEventListener("click", await copyToClipboard);

}