package webapp

import outwatch._
import outwatch.dsl._
import cats.effect.SyncIO
import colibri.{Observable, Subject}
import org.scalajs.dom.console

import scala.concurrent.duration.DurationInt

// Outwatch documentation:
// https://outwatch.github.io/docs/readme.html

object Main {
  def main(args: Array[String]): Unit =
    Outwatch.renderInto[SyncIO]("#app", app).unsafeRunSync()

  def app = div(
//    h1("Hello World!!!"),
//    counter,
//    inputField,
    initListOfLists
  )

  def counter = SyncIO {
    // https://outwatch.github.io/docs/readme.html#example-counter
    val number = Subject.behavior(0)
    div(
      button("+", onClick(number.map(_ + 1)) --> number, marginRight := "10px"),
      number,
    )
  }

  def inputField = SyncIO {
    // https://outwatch.github.io/docs/readme.html#example-input-field
    val text = Subject.behavior("")
    div(
      input(
        tpe := "text",
        value <-- text,
        onInput.value --> text,
      ),
      button("clear", onClick.as("") --> text),
      div("text: ", text),
      div("length: ", text.map(_.length)),
    )
  }

  def initListOfLists = SyncIO {
    val listOfLists = Subject.behavior(ul(`class` := "listOfLists"))
//    val listOfLists = Observable(ul(`class` := "listOfLists"))

    val buttonCreateList = button(
      "Create ToDo list",
      onClick(listOfLists.map(_.append(initElementList))) --> listOfLists,
      `class` := "buttonListCreate"
    )

    div(buttonCreateList, listOfLists)
  }

  def initElementList = SyncIO {
    val elementList = Subject.behavior(ul(`class` := "noteList"))

    val buttonCreateElement = button(
      "Create ToDo note",
      onClick(elementList.map(_.append(initElement))) --> elementList,
      `class` := "buttonNoteCreate"
    )

    val buttonDeleteList = button(
      "Delete ToDo list",
      onClick.asElement.foreach {
        element => element.parentNode.parentNode.removeChild(element.parentNode)
      },
      `class` := "buttonListRemove"
    )

    div(buttonCreateElement, buttonDeleteList, elementList)
  }

  def initElement = SyncIO {
    val buttonDeleteElement = button(
      "X",
      onClick.asElement.foreach {
        element => element.parentNode.parentNode.removeChild(element.parentNode)
      },
      `class`:="buttonNoteRemove"
    )

    val element = div(
      "Note text",
      buttonDeleteElement
    )

    element
  }

}
