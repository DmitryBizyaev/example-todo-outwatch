const {webDev} = require("@fun-stack/fun-pack");

module.exports = webDev({
  indexHtml: "src/main/html/index.html",
//  test-script: "src/main/html/test-script.js"
//   assetsDir: "src/main/html",
  extraStaticDirs: [
    "src", // for source maps
    "src/main/html/test-script.js",
    "src/main/html"
  ]
});

module.exports.devServer.host = "0.0.0.0";
module.exports.devServer.allowedHosts = "all";
